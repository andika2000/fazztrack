@extends('templates.index')

@section('title')
    Halaman Tambah Data
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/product/{{ $product->id }}" method="POST">
                @method('PUT')
                @csrf
                <div class="form-group">
                  <label for="nama_produk">Nama Barang</label>
                  <input type="text" value="{{ $product->nama_produk }}" name="nama_produk" class="form-control" id="nama_produk" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <input type="text" value="{{ $product->keterangan }}" name="keterangan" class="form-control" id="keterangan" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="harga">Harga</label>
                  <input type="text" value="{{ $product->harga }}" name="harga" class="form-control" id="harga" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="jumlah">Jumlah</label>
                  <input type="text" value="{{ $product->jumlah }}" name="jumlah" class="form-control" id="jumlah" aria-describedby="emailHelp">
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
@endsection

