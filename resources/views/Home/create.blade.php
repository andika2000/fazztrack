@extends('templates.index')

@section('title')
    Halaman Tambah Data
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="/product" method="POST">
                @csrf
                <div class="form-group">
                  <label for="nama_produk">Nama Barang</label>
                  <input type="text" name="nama_produk" class="form-control" id="nama_produk" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <input type="text" name="keterangan" class="form-control" id="keterangan" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="harga">Harga</label>
                  <input type="text" name="harga" class="form-control" id="harga" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                  <label for="jumlah">Jumlah</label>
                  <input type="text" name="jumlah" class="form-control" id="jumlah" aria-describedby="emailHelp">
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
@endsection

